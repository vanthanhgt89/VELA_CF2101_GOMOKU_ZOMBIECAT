const chai = require('chai');
const app = require('./app')
const expect = chai.expect;


describe('Caro', () => {
    describe('Win', () => {
        describe('Thắng 4 điểm không chặn', () => {
            it('Thắng 4 điểm không chặn chieu ngang', () => {
                const player1 = [
                    { x: 0, y: 1, value: 1 }, { x: 0, y: 2, value: 1 }, { x: 0, y: 3, value: 1 },
                    { x: 0, y: 4, value: 1 },
                ]
                let matrix = app.generate_matrix(10, 0);
                let updateMatrix = [], isWin;
                player1.forEach(item => {
                    updateMatrix = app.updateMatrix(matrix, item.x, item.y, item.value);
                    console.log(updateMatrix);
                    isWin = app.horizontal(updateMatrix, item.x, item.y, 1);
                })
                expect(isWin).to.equal(1)
            })

            it.only('Thắng 4 điểm không chặn chieu doc', () => {
                const player1 = [
                    { x: 5, y: 5, value: 1 }, { x: 2, y: 5, value: 1 }, { x: 3, y: 5, value: 1 },
                    { x: 4, y: 5, value: 1 },
                ]
                let matrix = app.generate_matrix(10, 0);
                let updateMatrix = [], isWin;
                player1.forEach(item => {
                    updateMatrix = app.updateMatrix(matrix, item.x, item.y, item.value);
                    console.log(matrix);
                    isWin = app.vertical(updateMatrix, item.x, item.y, 1);
                })
                expect(isWin).to.equal(1)
            })

            it('Thắng 4 điểm không chặn đường chéo trái', () => {
                const player1 = [
                    { x: 5, y: 5, value: 1 }, { x: 4, y: 4, value: 1 }, { x: 3, y: 3, value: 1 },
                    { x: 2, y: 2, value: 1 },
                ]
                let matrix = app.generate_matrix(10, 0);
                let updateMatrix = [], isWin;
                player1.forEach(item => {
                    updateMatrix = app.updateMatrix(matrix, item.x, item.y, item.value);
                    console.log(matrix);
                    isWin = app.diagonal(updateMatrix, item.x, item.y, 1);
                })
                expect(isWin).to.equal(1)
            })

            it.only('Thắng 4 điểm không chặn đường chéo phải', () => {
                const player1 = [
                    { x: 5, y: 5, value: 1 }, { x: 2, y: 5, value: 1 }, { x: 3, y: 5, value: 1 },
                    { x: 4, y: 5, value: 1 },
                ]
                let updateMatrix = [], isWin;
                player1.forEach(item => {
                    updateMatrix = app.updateMatrix(matrix, item.x, item.y, item.value);
                    console.log(matrix);
                    isWin = app.vertical(updateMatrix, item.x, item.y, 1);
                })
                expect(isWin).to.equal(1)
            })
        })


        it('Thắng 2 đường chéo 3 điểm không chặn', () => {

        })

        it('Thắng 4 chặn 1 đầu', () => {

        })

        it('Thắng 5 điểm không chặn', () => {

        })
    })

    describe('Error', () => {
        it('Không đánh trùng 1 điểm', () => {


        })
    })
})