const generate_matrix = (n, init) => {
  let mat = [];
  for (let i = 0; i < n; i++) {
    a = [];
    for (let j = 0; j < n; j++) {
      a[j] = init;
    }
    mat[i] = a;
  }
  return mat;
}

const updateMatrix = (matrix, x, y, value) => {
  matrix[x][y] = value;
  return matrix;
}

const horizontal = (mat, cur_row, cur_col, value) => {
  //di sang ben trai
  let count_left = 0;
  let count_right = 0
  let lastLeftCol = 0;
  let lastRightCol = 0;
  //Di sang phia ben trai so voi vi tri hien tai
  for (let i = cur_col; i >= 0; i--) {
    if (mat[cur_row][i] === value) {
      count_left++;
    } else {
      lastLeftCol = i;
      break;
    }

  }
  //Di sang phia ben phai so voi vi tri hien tai
  for (let j = cur_col + 1; j < 10; j++) {
    if (mat[cur_row][j] === value) {
      count_right++;
    } else {
      lastRightCol = j
      break;
    }

  }
  if (count_left + count_right >= 3 && mat[cur_row][lastLeftCol] === 0 && mat[cur_row][lastRightCol] === 0) {
    return 1
  }
  if (count_right + count_left >= 5) {
    return 1;
  }
}


const vertical = (mat, cur_row, cur_col, value) => {
  //di len tren
  let count_up = 0;
  let count_down = 0;
  let lastLeftCol = 0;
  let lastRightCol = 0;
  //Di len tren so voi vi tri hien tai
  for (let i = cur_row; i >= 0; i--) {
    if (mat[i][cur_col] === value) {
      count_up++;
      lastLeftCol = cur_row - count_up;
    }else {
      lastLeftCol = i
      break;
    }
  }
  //Di xuong duoi
  for (let j = cur_row + 1; j < 100; j++) {
    if (mat[j][cur_col] === value) {
      count_down++;
    }else {
      lastRightCol = j
      break;
    }
  }
  if (count_up + count_down >= 4 && mat[lastLeftCol][cur_col] === 0 && mat[lastRightCol][cur_col] === 0) {
    return 1;
  }
  if (count_down + count_up >= 5) {
    return 1;
  }
}

const diagonal = (mat, cur_row, cur_col, value) => {
  //di len tren
  let count_up = 0;
  let count_down = 0;
  let tempRow = cur_row;
  let tempCol = cur_col;
  //Di cheo len tren ben trai so voi vi tri hien tai
  for (let i = cur_row; i >= 0; i--) {
    if (mat[i][tempCol] === value) {
      tempCol--;
      count_up++;
    }
    else {
      break;
    }
  }
  tempCol = cur_col;
  //Di cheo xuong duoi ben phai
  for (let j = cur_row + 1; j < 100; j++) {
    if (mat[j][tempCol] === value) {
      count_down++;
      tempCol++;
    }
    else {
      break;
    }
  }
  if (count_up + count_down >= 5) {
    return 1;
  }
}

const diagonal_main = (mat, cur_row, cur_col, value) => {
  //di len tren
  let count_up = 0;
  let count_down = 0;
  let tempCol = cur_col;
  let tempRow = cur_row;
  //Di cheo len ben phai
  for (let i = cur_row; i >= 0; i--) {
    if (mat[i][tempCol] === value) {
      count_up++;
      tempCol++;
    }
    else {
      break;
    }
  }
  tempCol = cur_col
  //Di xuong duoi
  for (let j = cur_row + 1; j < 100; j++) {
    if (mat[j][tempCol] === value) {
      count_down++;
      tempCol--;
    }
    else {
      break;
    }
  }
  if (count_right + count_left >= 5) {
    return 1;
  }
}

module.exports = {
  generate_matrix,
  horizontal,
  vertical,
  diagonal,
  diagonal_main,
  updateMatrix

}